#include "cardpile.h"

CardPile::CardPile()
{
    // Seed the random generator
    srand((unsigned int)time(0));
}

void CardPile::print() {
    std::cout << "Printing out the card pile...\n";
    for(unsigned i = 0; i < this->m_cards.size(); i++) {
        std::cout << this->m_cards[i] << std::endl;
    }
}

int CardPile::size() const {
    return m_cards.size();
}

int CardPile::shuffle() {
    std::random_shuffle(m_cards.begin(), m_cards.end());
    return 0;
}

Card CardPile::top() const {
    return this->m_cards.front();
}

void CardPile::pop_top() {
    this->m_cards.pop_front();
}

bool CardPile::isEmpty() const {
    return this->m_cards.empty();
}

int CardPile::insertCardAtFront(const Card &card) {
    m_cards.push_front(card);
    return 0;
}

int CardPile::insertCardAtBack(const Card &card) {
    m_cards.push_back(card);
    return 0;
}

void CardPile::sortByOrder() {
    // Sort using cards' ordering value, rather than actual value.
    std::sort(this->m_cards.begin(), this->m_cards.end(), largerByOrder);
}

void CardPile::sortByValue() {
    // Sort using cards' actual value, rather than ordering value
    std::sort(this->m_cards.begin(), this->m_cards.end(), largerByValue);
}

int CardPile::calculateNumericValue() const {
    int numericValue = 0;
    for(unsigned card = 0; card < this->m_cards.size(); card++) {
        numericValue+=m_cards[card].getNumericValue();
    }
    return numericValue;
}

void CardPile::copy(const CardPile &pile) {
    this->clear();
    for(int card = 0; card < pile.size(); card++) {
        m_cards.push_back(pile.at(card));
    }
}

void CardPile::clear() {
    m_cards.clear();
}

Card CardPile::at(int index) const {
    return m_cards[index];
}
