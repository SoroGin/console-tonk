#include <vector>
#include <iostream>

#include <SFML/Graphics.hpp>

#include "cardpile.h"
#include "SFMLCard.h"

class SFMLCardPile : public sf::Drawable {
	enum Orientation {
		FACE_UP,
		FACE_DOWN
	};

	enum DisplayType {
		HORIZONTAL_SPREAD,
		VERTICAL_SPREAD,
		FAN,
		HORIZONTAL_STACKED,
		VERTICAL_STACKED
	};

	CardPile m_cardpile;
	std::vector<SFMLCard> m_cards;

	// Position of the card pile. Based on the position of the last card (when deck is face up)
	sf::Vector2f m_position;

	// Orientation of the card pile
	SFMLCardPile::Orientation m_orientation;
	// Display type of the card pile
	SFMLCardPile::DisplayType m_displaytype;

	void adjustCardsAsHorizontalSpread();
	
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	enum SFMLCardPileInitializationMethod {
		asEmptyDeck,
		as52CardDeck
	};

	// Initialize card pile with no cards
	SFMLCardPile();
	// Initialize card pile as a standard, ordered 52 card deck
	SFMLCardPile(const SFMLCardPile::SFMLCardPileInitializationMethod SFMLCardPileInitMethod);

	SFMLCard getSFMLCard(const int index) const;

	// Updates this class accordingly based on point given to it (mouse global position expected)
	void scanMousePosition(const sf::Vector2i point);

	void setPosition(sf::Vector2f position);

	void insertCardAtBack(Card card);
};