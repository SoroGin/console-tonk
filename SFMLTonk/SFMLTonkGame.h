#pragma once

#include <vector>

#include <SFML/Graphics.hpp>

#include "SFMLCardPile.h"
#include "SFMLCard.h"

// For OutputDebugStringA
#include <Windows.h>

class SFMLTonkGame
{
	// Non-event tied action processing, such as reading mouse positions
	int handleActions();
	int handleEvents();
	int handleDrawing();
	
	sf::RenderWindow mainGameWindow;
	// Main deck for the game
	SFMLCardPile deck;
	// Main discard pile for the game
	SFMLCardPile discardPile;
	// A vector of all the player hands
	std::vector<SFMLCardPile> playerHands;

	// Sprite for the tabletop
	sf::Sprite tableSprite;
	// Texture for the tabletop
	sf::Texture tableTexture;

	// Set up the playing table resources
	void setUpTable();

	// Testing, to delete
	SFMLCard card;
public:
	SFMLTonkGame();
	~SFMLTonkGame();

	// Runs the game. Returns exit code.
	int run();
};

