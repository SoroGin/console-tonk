#include "card.h"

Card::Card()
{
    this->m_rank = Card::Rank::UninitializedRank;
    this->m_suit = Card::Suit::UninitializedSuit;
}

Card::Card(Card::Rank rank, Card::Suit suit)
{
    this->m_rank = rank;
    this->m_suit = suit;

    // Assigns each card their ordering value to make it easier to sort
    this->orderingVal = rank + suit*13;

    // Assigns each card their weighted value (i.e. diamond < spade, ace < two)
    this->weightedVal = rank * 4 + suit;

    // Assigns each card their numeric value (i.e., Ace = 1, Two = 2... K = 10)
    if((rank + 1) > 10) {
        this->numericVal = 10;
    } else {
        this->numericVal = rank + 1;
    }
}

Card::Rank Card::getRank() const {
    return this->m_rank;
}

Card::Suit Card::getSuit() const {
    return this->m_suit;
}

int Card::getNumericValue() const {
    return this->numericVal;
}

bool Card::isSameRank(const Card &card) const {
    if(m_rank == card.getRank()) {
        return true;
    } else {
        return false;
    }
}

bool Card::isSameSuit(const Card &card) const {
    if(m_rank == card.getSuit()) {
        return true;
    } else {
        return false;
    }
}

std::ostream& operator<<(std::ostream& os, const Card& card) {
    std::cout << "|";
    switch(card.getRank()) {
    case Card::Rank::Ace:
        os << "A";
        break;
    case Card::Rank::Two:
        os << "2";
        break;
    case Card::Rank::Three:
        os << "3";
        break;
    case Card::Rank::Four:
        os << "4";
        break;
    case Card::Rank::Five:
        os << "5";
        break;
    case Card::Rank::Six:
        os << "6";
        break;
    case Card::Rank::Seven:
        os << "7";
        break;
    case Card::Rank::Eight:
        os << "8";
        break;
    case Card::Rank::Nine:
        os << "9";
        break;
    case Card::Rank::Ten:
        os << "10";
        break;
    case Card::Rank::Jack:
        os << "J";
        break;
    case Card::Rank::Queen:
        os << "Q";
        break;
    case Card::Rank::King:
        os << "K";
        break;
    case Card::Rank::UninitializedRank :
        os << "ERROR: Rank is unknown, or was never assigned!\n";
        break;
    default:
        os << "ERROR: Could not print card rank!\n";
        os << "enum value for card rank was: " << card.getRank();
        break;
    }

    os << "";

    switch(card.getSuit()) {
    case Card::Suit::Diamond :
        os << (char)4;
        break;
    case Card::Suit::Club :
        os << (char)5;
        break;
    case Card::Suit::Heart :
        os << (char)3;
        break;
    case Card::Suit::Spade :
        os << (char)6;
        break;
    case Card::Suit::UninitializedSuit :
        os << "ERROR: Suit is unknown, or was never assigned!\n";
        break;
    default:
        os << "ERROR: Could not print card suit!\n";
        os << "enum value for card suit was: " << card.getSuit();
        break;
    }
    std::cout << "|";
    return os;
}

void Card::copy(const Card &card) {
    m_suit = card.getSuit();
    m_rank = card.getRank();

    // Each card will have a unique value for ordering purposes
    // NOTE: This value is not indicative of actual value.
    orderingVal = getOrderingVal();

    // Card's actual value
    weightedVal = getVal();

    // Card's numeric value
    numericVal = getNumericValue();
}

int Card::getOrderingVal() const {
    return this->orderingVal;
}

int Card::getVal() const {
    return this->weightedVal;
}

bool largerByOrder(const Card& a, const Card& b) {
    return a.getOrderingVal() <= b.getOrderingVal();
}

bool largerByValue(const Card &a, const Card &b) {
    return a.getVal() <= b.getVal();
}
