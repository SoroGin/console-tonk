
#include "client.h"

Client::Client(char* remoteIP)
{
    ip = remoteIP;
}

int Client::setupConnection() {
    std::cout << "\nPreparing client ..." << std::endl;
    if(initialize() > 0) {
        std::cerr << "ERROR: Unable to intialize client." << std::endl;
        return 1;
    }

    if(resolveAddress() > 0) {
        std::cerr << "ERROR: Unable to resolve address." << std::endl;
        return 1;
    }

    if(connectToSocket() > 0) {
        std::cerr << "ERROR: Unable to connect to socket." << std::endl;
        return 1;
    }

    return 0;
}

int Client::connectToSocket() {
    // Attempt to connect to an address until one succeeds
    for(struct addrinfo *ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        connectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (connectSocket == INVALID_SOCKET) {
            std::cout << "socket failed with error: " << WSAGetLastError() << std::endl;
            WSACleanup();
            return 1;
        }

        // Connect to server.
        int status = connect( connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (status == SOCKET_ERROR) {
            std::cout << "connect failed with error: " << WSAGetLastError() << std::endl;
            WSACleanup();

            closesocket(connectSocket);
            connectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (connectSocket == INVALID_SOCKET) {
        std::cout << "Unable to connect to server!" << std::endl;
        WSACleanup();
        return 1;
    }

    std::cout << "Connected to server.\n" << std::endl;
    return 0;
}

int Client::cleanup() {

    // cleanup
    closesocket(connectSocket);
    WSACleanup();

    //std::cout << "Cleanup completed." << std::endl;
    return 0;
}

