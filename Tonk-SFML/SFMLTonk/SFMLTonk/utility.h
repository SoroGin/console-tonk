#include <SFML\Graphics.hpp>

namespace kf {
	namespace utility {
		bool withinSprite(const sf::Sprite &sprite, const sf::Vector2f &pos);
	}
}