#include "SFMLTonkGame.h"

SFMLTonkGame::SFMLTonkGame() 
	: mainGameWindow(sf::VideoMode(1280, 720), "Tonk!", sf::Style::Close), card(Card::Rank::Nine, Card::Suit::Heart),
	deck(SFMLCardPile::SFMLCardPileInitializationMethod::as52CardDeck)
{
	setUpTable();

	// testing to delete
	card.setPosition(sf::Vector2f(mainGameWindow.getSize().x / 2 - card.getGlobalBounds().width / 2, mainGameWindow.getSize().y / 2 - card.getGlobalBounds().height / 2));
}


SFMLTonkGame::~SFMLTonkGame()
{
}

void SFMLTonkGame::setUpTable() {
	if (!tableTexture.loadFromFile(".\\Resources\\Images\\Table\\Background.png")) {
		OutputDebugStringA("ERROR: Background.png could not load. The game will now exit.\n");
		exit(-1);
	}
	tableSprite.setTexture(tableTexture);
	tableSprite.setScale(sf::Vector2f(.67, .67));
}

int SFMLTonkGame::handleEvents() {
	int returnValue = 0;
	sf::Event event;
	while (mainGameWindow.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			mainGameWindow.close();
	}
	return returnValue;
}

int SFMLTonkGame::handleActions() {
	int returnValue = 0;
	card.scanMousePosition(sf::Mouse::getPosition(mainGameWindow));
	deck.scanMousePosition(sf::Mouse::getPosition(mainGameWindow));
	return returnValue;
}

int SFMLTonkGame::handleDrawing() {
	int returnValue = 0;
	mainGameWindow.clear();
	mainGameWindow.draw(tableSprite);
	mainGameWindow.draw(card);
	mainGameWindow.draw(deck);
	mainGameWindow.display();
	return returnValue;
}

int SFMLTonkGame::run() {
	int returnValue = 0;
	while (mainGameWindow.isOpen()) {
		returnValue = handleActions();
		returnValue = handleEvents();
		returnValue = handleDrawing();
		if (returnValue != 0)
			break;
	}
	return returnValue;
}
