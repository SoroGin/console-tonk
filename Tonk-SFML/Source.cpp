#include <SFML/Graphics.hpp>

#include <iostream>
#include <stdlib.h>

#include "SFMLTonkGame.h"
#include "SFMLCard.h"

#include <Windows.h>

int main()
{
	SFMLTonkGame game;
	return game.run();
}