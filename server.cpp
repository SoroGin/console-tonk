#include "server.h"

Server::Server()
{
    listenSocket = INVALID_SOCKET;
}

int Server::setupConnection() {
    std::cout << "\nPreparing server ..." << std::endl;
    if(initialize() > 0) {
        std::cerr << "ERROR: Unable to intialize server." << std::endl;
        return 1;
    }

    if(resolveAddress() > 0) {
        std::cerr << "ERROR: Unable to resolve address." << std::endl;
        return 1;
    }

    if(getMyIP() > 0) {
        std::cerr << "ERROR: Unable to obtain your IPv4 address. Please attempt to do so manually." << std::endl;
    }

    if(createSocket() > 0) {
        std::cerr << "ERROR: Unable to create socket." << std::endl;
        return 1;
    }

    if(bindSocket() > 0) {
        std::cerr << "ERROR: Unable to bind to socket." << std::endl;
        return 1;
    }

    if(listenOnSocket() > 0) {
        std::cerr << "ERROR: Unable to listen on socket." << std::endl;
        return 1;
    }

    // wait for client
    if(acceptClientConnection() > 0) {
        std::cerr << "ERROR: Unable to accept client socket." << std::endl;
        return 1;
    }

    return 0;
}

int Server::createSocket() {
    listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (listenSocket == INVALID_SOCKET) {
        std::cout << "Error at socket(): " << WSAGetLastError() << std::endl;
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }
    std::cout << "Socket created." << std::endl;
    return 0;
}

int Server::bindSocket() {
    // Setup the TCP listening socket
    int status = bind( listenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (status == SOCKET_ERROR) {
        std::cout << "bind failed with error: " << WSAGetLastError() << std::endl;
        freeaddrinfo(result);
        closesocket(listenSocket);
        WSACleanup();
        return 1;
    }

    freeaddrinfo(result);
    std::cout << "Socket bound." << std::endl;
    return 0;
}

int Server::listenOnSocket() {
    int status = listen(listenSocket, SOMAXCONN);
    if (status == SOCKET_ERROR) {
        std::cout << "listen failed with error: " << WSAGetLastError() << std::endl;
        closesocket(listenSocket);
        WSACleanup();
        return 1;
    }
    std::cout << "Waiting for client ..." << std::endl;
    return 0;
}

int Server::acceptClientConnection() {
    // Accept a client socket
    connectSocket = accept(listenSocket, NULL, NULL);
    if (connectSocket == INVALID_SOCKET) {
        std::cout << "accept failed: " << WSAGetLastError() << std::endl;
        closesocket(listenSocket);
        WSACleanup();
        return 1;
    }
    std::cout << "Connection established.\n" << std::endl;
    return 0;
}

int Server::cleanup() {
    // cleanup
    closesocket(listenSocket);
    closesocket(connectSocket);
    WSACleanup();

    //std::cout << "Cleanup completed." << std::endl;
    return 0;
}
