#include "SFMLCard.h"

SFMLCard::SFMLCard(Card card) : SFMLCard(card.getRank(), card.getSuit()) {
}

SFMLCard::SFMLCard(Card::Rank r, Card::Suit s) : Card(r, s) {
	std::string filenameOfCardImage;

	switch (this->getRank()) {
	case Card::Rank::Ace:
		filenameOfCardImage.append("Ace");
		break;
	case Card::Rank::Two:
		filenameOfCardImage.append("2");
		break;
	case Card::Rank::Three:
		filenameOfCardImage.append("3");
		break;
	case Card::Rank::Four:
		filenameOfCardImage.append("4");
		break;
	case Card::Rank::Five:
		filenameOfCardImage.append("5");
		break;
	case Card::Rank::Six:
		filenameOfCardImage.append("6");
		break;
	case Card::Rank::Seven:
		filenameOfCardImage.append("7");
		break;
	case Card::Rank::Eight:
		filenameOfCardImage.append("8");
		break;
	case Card::Rank::Nine:
		filenameOfCardImage.append("9");
		break;
	case Card::Rank::Ten:
		filenameOfCardImage.append("10");
		break;
	case Card::Rank::Jack:
		filenameOfCardImage.append("Jack");
		break;
	case Card::Rank::Queen:
		filenameOfCardImage.append("Queen");
		break;
	case Card::Rank::King:
		filenameOfCardImage.append("King");
		break;
	default:
		OutputDebugStringA("ERROR: Problem with creating file name for card image.\n");
		break;
	}

	filenameOfCardImage.append("-");

	switch (this->getSuit()) {
	case Card::Suit::Diamond:
		filenameOfCardImage.append("Diamonds");
		break;
	case Card::Suit::Club:
		filenameOfCardImage.append("Clubs");
		break;
	case Card::Suit::Heart:
		filenameOfCardImage.append("Hearts");
		break;
	case Card::Suit::Spade:
		filenameOfCardImage.append("Spades");
		break;
	default:
		OutputDebugStringA("ERROR: Problem with creating file name for card image.\n");
		break;
	}

	filenameOfCardImage.append(".png");

	std::string pathToCardImage = ".\\Resources\\Images\\Cards\\" + filenameOfCardImage;
	if (!m_texture.loadFromFile(pathToCardImage)) {
		OutputDebugStringA("ERROR: Problem with accessing card image file.\n");
		OutputDebugStringA(pathToCardImage.c_str());
		OutputDebugStringA("\n");
		exit(-1);
	}
	m_sprite.setTexture(m_texture);
	m_sprite.setScale(.2, .2);

	setPosition(sf::Vector2f(0, 0));
	m_glow.setSize(sf::Vector2f(m_sprite.getGlobalBounds().width + 10, m_sprite.getGlobalBounds().height + 10));

	m_glow.setFillColor(sf::Color::Transparent);
}

// Copy constructor
SFMLCard::SFMLCard(const SFMLCard& card) : m_texture(card.getTexture())
{
	m_sprite.setTexture(m_texture);
	m_sprite.setScale(.2, .2);

	setPosition(card.getPosition());
	m_glow.setSize(sf::Vector2f(m_sprite.getGlobalBounds().width + 10, m_sprite.getGlobalBounds().height + 10));

	m_glow.setFillColor(sf::Color::Transparent);
}

//Copy assignment operator
SFMLCard& SFMLCard::operator = (const SFMLCard& card) 
{
	return SFMLCard(card);
}

// Deconstructor
SFMLCard::~SFMLCard()
{

}

void SFMLCard::setPosition(const sf::Vector2f& pos) {
	m_sprite.setPosition(pos);
	m_glow.setPosition(pos.x - 5, pos.y - 5);
}

void SFMLCard::setGlow(bool setToGlow) {
	if (setToGlow) {
		sf::Color glowBlue(0, 0, 255, 25);
		m_glow.setFillColor(glowBlue);
	}
	else {
		m_glow.setFillColor(sf::Color::Transparent);
	}
}

sf::FloatRect SFMLCard::getGlobalBounds() const {
	return m_sprite.getGlobalBounds();
}

void SFMLCard::scanMousePosition(const sf::Vector2i point) {
	if (this->getGlobalBounds().contains(sf::Vector2f(point))) {
		this->setGlow(true);
	}
	else {
		this->setGlow(false);
	}
}

sf::Vector2f SFMLCard::getPosition() const {
	return m_sprite.getPosition();
}

sf::Sprite SFMLCard::getSprite() const {
	return m_sprite;
}

const sf::Texture& SFMLCard::getTexture() const {
	return m_texture;
}

void SFMLCard::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_glow);
	target.draw(m_sprite, states);
}