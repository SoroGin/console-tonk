#ifndef HAND_H
#define HAND_H

// guarantees that inherited member variables of base classes are correctly serialized
#include <boost/serialization/base_object.hpp>

#include "cardpile.h"
#include "card.h"

/* This card represents a player hand */

class Hand : public CardPile
{
public:
    Hand();

    // Retrieves card at given position and removes it from hand. Index starts at 0.
    Card getCard(int index);

    // Returns card at given position
    Card at(int index);

    // Marks this player as the winner of the game
    void setWinner();

    // Returns true if player has won the game, false otherwise
    bool getWinner();

    // Prints all cards of hand to console in one line
    virtual void print();

private:
    // Allow serialization to access non-public data members.
    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive& ar, const unsigned version)
    {
        // serialize base class information
        ar & boost::serialization::base_object<CardPile>(*this);
        ar & isWinner;
    }

    bool isWinner;
};

#endif // HAND_H
