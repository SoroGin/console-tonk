#include "tonkconsolegameapp.h"

TonkConsoleGameApp::TonkConsoleGameApp()
{
}

// This current implementation of this function is not fully robust, as it allows for a case such as
// 123asdfajkdks and reads it as 123
int TonkConsoleGameApp::retrieveInt() const {
    int number;
    while(!(std::cin >> number)) {
        std::cin.clear();
        // QUESTION: This started complaining out of the blue. Well not completely out of the blue, it was after I hooked in the networking stuff.
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Sorry! That wasn't a valid input. Make sure to enter a number and try again!: ";
    }
    return number;
}

int TonkConsoleGameApp::retrieveNumPlayers() {
	// Decide how many players there will be
	std::cout << "How many players will be playing? (between 2-4): ";
	// Limit the number of players to a minimum of 2 but a maximum of 4
	// Retrieve an integer from the user and set it as numplayers
	return retrieveIntInRange(2, 4);
}

int TonkConsoleGameApp::retrieveIntInRange(const int &min, const int &max) const {
    int number = retrieveInt();
    while(number < min || number > max) {
        std::cout << "Sorry! That wasn't a valid choice. Try again!: ";
        number = retrieveInt();
    }
    return number;
}

void TonkConsoleGameApp::retrieveIP(char* remoteIP) {
    std::cout << "Please enter the remote server's IPv4 address: ";
    while (!(std::cin >> remoteIP)) {
        std::cin.clear();
        std::cout << "Something went wrong. Please try again." << std::endl;
    }
}

int TonkConsoleGameApp::readNumberString(std::string str, std::vector<int> &ints) {
    // Verify that the input is a list of comma delimited integers. Spaces after commas are optional.
    if (!std::regex_match(str, std::regex("^(\\d+(,\\s?\\d+)*)$") )) {
        std::cout << "ERROR: Input string does not match expected pattern." << std::endl;
        return 1;
    }

    // Remove spaces from string. Helps avoid issues when parsing
    str.erase(remove_if(str.begin(), str.end(), isspace), str.end());

    // Split string using comma as delimiter and populate 'ints' vector
    std::stringstream ss(str);
    std::string item;
    while (std::getline(ss, item, ',')) {
        ints.push_back(atoi(item.c_str()));
    }

    std::sort(ints.begin(), ints.end());
    return 0;
}

int TonkConsoleGameApp::getPlayerChoiceForSpreadCreation() {
    std::cout << "Would you like to create a new spread(1), add to a spread(2), view spreads(3) or continue(4)?";
    return retrieveIntInRange(1,4);
}

int TonkConsoleGameApp::findIndexOfLowestHand(const std::vector<Hand> &hand) const {
    // Max hand value is 50 because 10 x 5 cards.
    // TO-DO: FIX THIS MAYBE. HAND SIZE MIGHT CHANGE DEPENDING ON NUMBER OF PLAYERS
    const int MAXHANDVALUE = 50;
    int lowestHand = MAXHANDVALUE;
    int indexOfLowestHand = -1;
    for(unsigned player = 0; player < hand.size(); player++) {
        int numericValueOfPlayer = hand[player].calculateNumericValue();
        if(lowestHand > numericValueOfPlayer) {
            lowestHand = numericValueOfPlayer;
            indexOfLowestHand = player;
        }
    }
    return indexOfLowestHand;
}

int TonkConsoleGameApp::sendState(Connection &connection) {
    // Serialize all decks and save them to archive
    std::stringstream outgoingStream;
    {
        boost::archive::text_oarchive outgoingArchive(outgoingStream);
        outgoingArchive & m_deck & m_discard & m_hands & m_spreads;
    }

    // Send the serialized object 
    if (connection.sendMessage(outgoingStream.str().c_str()) > 0) {
        std::cerr << "ERROR: Unable to send serialized object." << std::endl;
        return 1;
    }

    return 0;
}

int TonkConsoleGameApp::loadState(Connection &connection) {
    std::stringstream incomingStream;

    // Listen for incoming serialized decks
    if (connection.receiveMessage() > 0) {
        std::cerr << "ERROR: Unable to receive serialized object." << std::endl;
        return 1;
    }

    // Deserialize all incoming decks
    incomingStream.str(connection.getReceiveBuffer());
    {
        boost::archive::text_iarchive incomingArchive(incomingStream);
        incomingArchive & m_deck & m_discard & m_hands & m_spreads;
    }

    // clear receiveBuffer to prepare for next incoming message
    connection.clearBuffer();

    return 0;
}

void TonkConsoleGameApp::createHandsForPlayers() {
    for (int i = 0; i < m_numplayers; i++) {
        Hand newHand;
        m_hands.push_back(newHand);
    }
}


void TonkConsoleGameApp::dealCards() {
    // Deal out the cards to the players
    for (unsigned player = 0; player < m_hands.size(); player++) {
        // Will deal out five cards
        for (int cardsToDeal = 0; cardsToDeal < 5; cardsToDeal++) {
            if (!m_deck.isEmpty()) {
                m_hands[player].insertCardAtFront(m_deck.top());
                m_deck.pop_top();
            }
            else {
                std::cerr << "ERROR: Something went wrong with the game. It tried to draw from an empty deck!" << std::endl;
            }
        }
    }
}

int TonkConsoleGameApp::handleTonk() {
    std::cout << "************************************************************" << std::endl;
    std::cout << "********************   You've tonked!   ********************" << std::endl;
    std::cout << "************************************************************" << std::endl;

    // print hands of each player
    for (unsigned player = 0; player < m_hands.size(); player++) {
        int playerNumber = player + 1;
        std::cout << "Player " << playerNumber << "'s hand was: " << std::endl;
        m_hands[player].print();
        std::cout << std::endl;
        std::cout << "And had a value of: " << m_hands[player].calculateNumericValue();
        std::cout << std::endl << std::endl;
    }

    int indexOfPlayerWithLowestHand = findIndexOfLowestHand(m_hands);
    int playerNumberWithLowestHand = indexOfPlayerWithLowestHand + 1;

    return indexOfPlayerWithLowestHand;
}

int TonkConsoleGameApp::runAsHost() {
	Server server;

	// Initialize server and wait for a client
	if (server.setupConnection() > 0) {
		std::cerr << "ERROR: Unable to setup connection." << std::endl;
		return 1;
	}

	// At this point, the server has established a connection with a client.
	// This also means that we now have 2 players, the host and the remote user
	m_numplayers = 2;

    std::cout << "Alright, I'll start the game for " << m_numplayers << " players!" << std::endl << std::endl;

    // Create the number of players and start tracking them
    createHandsForPlayers();

	// Shuffle the deck to prep it for play
	m_deck.shuffle();

    dealCards();

	// Start discard pile off with a card from the deck
	if (!m_deck.isEmpty()) {
		m_discard.insertCardAtFront(m_deck.top());
		m_deck.pop_top();
	}
	else {
		std::cerr << "ERROR: Something went wrong with the game. It tried to draw from an empty deck!" << std::endl;
	}

	m_running = true;
	// While the game is still going on, keep processing turns but ONLY FOR THE HOST PLAYER.
	// Once the host player has made his turn, his moves will be relayed to the other player via the XML state file.
	// The host player will then wait for his game to be updated once the other player finishes his turn.
	// And round and round we go
	while (m_running) {

        // Check if you won from client player's tonk
        if(m_hands[0].getWinner()) {
            std::cout << "Congrats! You win!\n" << std::endl;
            m_running = false;
        }

        // Check if the client won on his last turn
        else if(m_hands[1].getWinner()) {
            // nice...
            std::cout << "Looks like your buddy kicked your ass!\nBetter luck next time chump.\n" << std::endl;
            m_running = false;
        }

        else {
            // The game is capped at 2 players for now. The host will be player 1 and his hand will be in index 0 of m_hands.
            // Since we wait until a client connects to the host, we can be certain that there will be at least 2 players by this point
            int whatToDoAfterTurn = processTurn(m_hands[0], 0);
            // You won!
            if (whatToDoAfterTurn == 1) {
                m_hands[0].setWinner();
                sendState(server);
            }
            // Tonked so check for winner
            else if (whatToDoAfterTurn == 2) {
                int indexOfPlayerWithLowestHand = handleTonk();
                m_hands[indexOfPlayerWithLowestHand].setWinner();
                sendState(server);
            }
            // Game is still on. Keep going
            else {
                sendState(server);
                loadState(server);
            }
        }
            
	}

    // Destroy connection gracefully
    if(server.shutdownConnection() > 0) {
        std::cerr << "ERROR: Unable to properly shutdown connection." << std::endl;
        return 1;
    }
	return 0;
}

int TonkConsoleGameApp::runAsClient() {
    char hostIP[16];
    retrieveIP(hostIP);
    Client client(hostIP);

	// Initialize client and attempt to connect to server
	if (client.setupConnection() > 0) {
		std::cerr << "ERROR: Unable to setup connection." << std::endl;
		return 1;
    }

    // At this point, the server has established a connection with a client.
    // This also means that we now have 2 players, the host and the remote user
    m_numplayers = 2;

    std::cout << "Please wait for your buddy to make the first move.\n" << std::endl;
    loadState(client);

    // Both players's games should be initialized and in sync so let's play!
    m_running = true;

	// Loop until someone wins. Remember, this loop only cares about the client player
	while (m_running) {
        // Check if the host won on his last turn
        if(m_hands[0].getWinner()) {
            std::cout << "Looks like your buddy kicked your ass!\nBetter luck next time chump.\n" << std::endl;
            m_running = false;
        }

        // Check if you won from host player's tonk
        else if(m_hands[1].getWinner()) {
            std::cout << "Congrats! You win!\n" << std::endl;
            m_running = false;
        }

        else {
            // The game is capped at 2 players for now. The client will be player 2 and his hand will be in index 1 of m_hands.
            // Since we wait until a client connects to the host, we can be certain that there will be at least 2 players by this point.
            int whatToDoAfterTurn = processTurn(m_hands[1], 1);
            // You won!
            if (whatToDoAfterTurn == 1) {
                m_hands[1].setWinner();
                sendState(client);
            }
            // Tonked so check for winner
            else if (whatToDoAfterTurn == 2) {
                int indexOfPlayerWithLowestHand = handleTonk();
                m_hands[indexOfPlayerWithLowestHand].setWinner();
                sendState(client);
            }
            // Game is still on. Keep going
            else {
                sendState(client);
                loadState(client);
            }
        }
	}

    // Destroy connection gracefully
    if(client.shutdownConnection() > 0) {
        std::cerr << "ERROR: Unable to properly shutdown conneciton." << std::endl;
        return 1;
    }
	return 0;
}


int TonkConsoleGameApp::runOffline() {
	m_numplayers = retrieveNumPlayers();

	std::cout << "Alright, I'll start the game for " << m_numplayers << " players!" << std::endl << std::endl;

	// Create the number of players and start tracking them
	for (int i = 0; i < m_numplayers; i++) {
		Hand newHand;
		m_hands.push_back(newHand);
	}

	// Shuffle the deck to prep it for play
	m_deck.shuffle();

    dealCards();

	// Start discard pile off with a card from the deck
	if (!m_deck.isEmpty()) {
		m_discard.insertCardAtFront(m_deck.top());
		m_deck.pop_top();
	}
	else {
		std::cerr << "ERROR: Something went wrong with the game. It tried to draw from an empty deck!" << std::endl;
	}

	// Whether or not the game is running
	m_running = true;

	// Loop until someone wins. 
	while (m_running) {
		for (unsigned player = 0; player < m_hands.size(); player++) {
            std::cout << ":::::::::::::::::::::::::::::::::::::::::::::" << std::endl << std::endl;

            // Tell the player his / her number!
			int playerNumber = player + 1;
            std::cout << "Hello player " << playerNumber << "." << std::endl << std::endl;
            int whatToDoAfterTurn = processTurn(m_hands[player], player);


            // TO-DO: The game ends if the deck is out of cards.


            // If player has won, set m_running to false
			if (whatToDoAfterTurn == 1) {
				m_running = false;
			}
			else if (whatToDoAfterTurn == 2) {
                handleTonk();
				return 0;
			}
		}
	}

	return 0;
}

int TonkConsoleGameApp::run() {
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::*************   Welcome  **************::::" << std::endl;
	std::cout << "::::***************   To    ***************::::" << std::endl;
	std::cout << "::::*************    Tonk!   **************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;

	std::cout << "Please say whether you'd like to play offline(1) or online(2): ";
	int offlineOnlineChoice = retrieveIntInRange(1, 2);

	// offline
	if (offlineOnlineChoice == 1) {
		runOffline();
	}
	// online
	else if (offlineOnlineChoice == 2) {
		// Users can decide between hosting the game (server) or joining an existing game (client)
		m_connectionType = -1;
        std::cout << "Would you like to host a new game(1) or join an existing game(2)? ";
        m_connectionType = retrieveIntInRange(1, 2);

		// Setup game as the host
		if (m_connectionType == 1) {
			runAsHost();
		}
		else {
			runAsClient();
		}
	}

	int empty;
	std::cin >> empty;
    // TO-DO: Ask player if he / she wants to play again, and if so, relaunch the app
    return 0;
}

int TonkConsoleGameApp::processTurn(Hand &hand, const int playerNum) {
    // -1. Show the player his hand
    // Sort the player's hand first
    std::cout << "Your hand is: ";
    hand.print();
    std::cout << std::endl;

    // 0. Verify that neither the discard nor the deck is empty
    if(m_discard.isEmpty()) {
        // The discard pile should never be empty at this point, so throw an error if it is!
        std::cerr << "ERROR: For some reason, the discard pile has nothing in it!" << std::endl;
    }

    if(m_deck.isEmpty()) {
        // Handle the case for when the deck is empty!
        // When the deck runs out, it's an automatic Tonk! Lowest wins!
        return 2;
    }

    // 1. Draw a card either from discard pile or deck.
    std::cout << "The card that's face up on the discard pile is: " << m_discard.top() << std::endl << std::endl;
    std::cout << "Do you want to draw from the discard pile(1) or the deck(2), or tonk(3)?: ";

    int choice;
    // The choice can only be either 1, draw from discard, 2, draw from deck, or 3, tonk / drop
    while(!(choice = retrieveIntInRange(0,3))) {
        std::cout << "Sorry! Try again, and make sure your choice is either draw from discard(1) or draw from deck(2) or tonk(3): ";
    }

    switch(choice) {
    case 1:
        // Give requested card to player
        std::cout << "Alright, I'll give you the card ";
        std::cout << "on the top of discard!: " << m_discard.top() << std::endl;
        // Player chose to draw from discard
        // Give the player the card
        hand.insertCardAtFront(m_discard.top());
        // Make sure the given card is no longer in the deck
        m_discard.pop_top();
        break;
    case 2:
        // Give requested card to player
        std::cout << "Alright, I'll give you the card ";
        std::cout << "from the top of the deck!: " << m_deck.top() << std::endl;
        // Player chose to draw from deck
        // Give the player the card
        hand.insertCardAtFront(m_deck.top());
        // Make sure the given card is no longer in the deck
        m_deck.pop_top();
        break;
    case 3:
        // Tonked
        return 2;
        break;
    default:
        std::cerr << "ERROR: Something went wrong when I tried to give you a card!" << std::endl;
        break;
    }
\
    // 2. TO-DO: Handle creating any spreads player might want to create
    bool creatingSpread = true;
    while(creatingSpread) {
        int choice = getPlayerChoiceForSpreadCreation();
        std::vector<int> cardsForNewSpread;
        std::string stringOfCardsForNewSpread;
        Spread newCreatedSpread;
        bool cardsForNewSpreadOutOfRange = true;

        switch(choice) {
        // New spread
        case 1:
            std::cout << "Your hand is ";
            hand.print();
            std::cout << "Enter the cards you want to enter into a new spread: ";
            std::cin >> stringOfCardsForNewSpread;

            while(cardsForNewSpreadOutOfRange){
                bool errorOccurred = false;
                // if garbage entered, or there weren't enough cards entered
                if(readNumberString(stringOfCardsForNewSpread, cardsForNewSpread) != 0 || cardsForNewSpread.size() < 3) {
                    errorOccurred = true;
                }
                // Check each card player chose for spread
                if(!errorOccurred) {
                    for(unsigned i = 0; i < cardsForNewSpread.size();i++) {
                        // If the cards for the new spread are out of range
                        if(cardsForNewSpread[i] < 0 || cardsForNewSpread[i] > hand.size()) {
                            errorOccurred = true;
                        } else {
                            cardsForNewSpreadOutOfRange = false;
                        }
                    }
                }
                if (errorOccurred == true) {
                    std::cout << "An incorrect string was entered! Please enter new number choices: ";
                    cardsForNewSpread.clear();
                    stringOfCardsForNewSpread.clear();
                    std::cin >> stringOfCardsForNewSpread;
                }

            }

            // Try to create spread based on numbers the user enters
            for(unsigned i = 0; i < cardsForNewSpread.size(); i++) {
                Card card(Card::Rank::Ace, Card::Suit::Spade);
                card = hand.at(cardsForNewSpread[i] - 1);
                // If an invalid card was added
                if(newCreatedSpread.insertCardAtBack(card) == -1) {
                    std::cout << "That's not a valid spread!" << std::endl << std::endl;
                    int newCreatedSpreadSize = newCreatedSpread.size();
                    for(int j = 0; j < newCreatedSpreadSize; j++) {
                        hand.insertCardAtFront(newCreatedSpread.top());
                        newCreatedSpread.pop_top();
                    }
                    break;
                } else {
                    hand.getCard(cardsForNewSpread[i]-1);
                    // Adjust all the numbers in the cardsForNewSpread
                    for(unsigned j = 0; j < cardsForNewSpread.size(); j++) {
                        cardsForNewSpread[j]--;
                    }
                }
            }
            if(newCreatedSpread.size() > 0) {

                newCreatedSpread.print();
                newCreatedSpread.setPlayer(playerNum);
                m_spreads.push_back(newCreatedSpread);
            }
            break;
        // Add to pre-existing spread WARNING: THIS CODE MIGHT NOT WORK. HAS NOT BEEN TESTED
        case 2:
            std::cout << "Which spread would you like to add to?" << std::endl;
            int indexOfSpreadToAddCardsTo;
            for(unsigned i = 0; i < m_spreads.size(); i++) {
                std::cout << "Spread " << i;
                m_spreads[i].print();
            }
            indexOfSpreadToAddCardsTo = retrieveIntInRange(0, m_spreads.size());
            std::cout << "Your hand is ";
            hand.sortByValue();
            hand.print();
            std::cout << "Enter the cards you want to enter into a new spread: ";
            stringOfCardsForNewSpread.clear();
            std::cin >> stringOfCardsForNewSpread;
            cardsForNewSpreadOutOfRange = true;

            while(cardsForNewSpreadOutOfRange) {
                bool errorOccurred = false;
                // if garbage entered, or there weren't enough cards entered
                if(readNumberString(stringOfCardsForNewSpread, cardsForNewSpread) != 0 || cardsForNewSpread.size() < 3) {
                    errorOccurred = true;
                }
                // Check each card player chose for spread
                if(!errorOccurred) {
                    for(unsigned i = 0; i < cardsForNewSpread.size();i++) {
                        if(cardsForNewSpread[i] < 0 || cardsForNewSpread[i] > hand.size()) {
                            std::cout << "An invalid number was entered! Please enter new number choices." << std::endl;
                            cardsForNewSpread.clear();
                            stringOfCardsForNewSpread.clear();
                            std::cin >> stringOfCardsForNewSpread;
                        } else {
                            cardsForNewSpreadOutOfRange = false;
                        }
                    }
                }
                if (errorOccurred == true) {
                    std::cout << "An incorrect string was entered! Please enter new number choices: ";
                    cardsForNewSpread.clear();
                    stringOfCardsForNewSpread.clear();
                    std::cin >> stringOfCardsForNewSpread;
                }
            }

            for(unsigned i = 0; i < cardsForNewSpread.size(); i++) {
                Card card(Card::Rank::Ace, Card::Suit::Spade);
                card = hand.at(cardsForNewSpread[i] - 1);
                if(m_spreads[indexOfSpreadToAddCardsTo].insertCardAtFront(card) == -1) {
                    std::cout << "inserting a card failed. Reverting and breaking." << std::endl;
                    int newCreatedSpreadSize = m_spreads[indexOfSpreadToAddCardsTo].size();
                    for(int j = 0; j < newCreatedSpreadSize; j++) {
                        hand.insertCardAtFront(m_spreads[indexOfSpreadToAddCardsTo].top());
                        m_spreads[indexOfSpreadToAddCardsTo].pop_top();
                    }
                    hand.sortByValue();
                    break;
                } else {
                    std::cout << "card inserted successfully" << std::endl;
                    hand.getCard(cardsForNewSpread[i]-1);
                    // Adjust all the numbers in the cardsForNewSpread
                    for(unsigned j = 0; j < cardsForNewSpread.size(); j++) {
                        cardsForNewSpread[j]--;
                    }
                }
            }

            break;
        // view all spreads
        case 3:
            if(m_spreads.size() <= 0) {
                std::cout << "There are no spreads!" << std::endl << std::endl;
            }
            for(unsigned spread = 0; spread < m_spreads.size(); spread++) {
                std::cout << "player " << m_spreads[spread].getPlayer() << " has this spread" << std::endl;
                m_spreads[spread].print();
            }
            break;
        // Don't create spread / i.e. continue
        default:
            creatingSpread = false;
            break;
        }
    }

    // TO-DO: Add to pre-existing spread

    // 3. Discard a card.
    std::cout << "Choose a card to discard!" << std::endl;
    std::cout << "Your hand is: ";
    hand.print();

    // initialize
    while((choice = retrieveInt()) < 0 || choice > hand.size()) {
        std::cout << "Sorry, that's not a valid number. Try again: ";
    }

    // Adjust player choice into appropriate hand index (which starts at 0)
    int hand_index = choice - 1;

    // Insert the player chosen card into discard
    m_discard.insertCardAtFront(hand.getCard(hand_index));

    // Sort the player's hand
    hand.sortByValue();

    // 4. Check if player has won.
    if(hand.size() == 0) {
        std::cout << "You've won!" << std::endl;
        hand.setWinner();
        return 1;
    }

    return 0;
}
