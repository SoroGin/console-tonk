#ifndef TONKCONSOLEGAMEAPP_H
#define TONKCONSOLEGAMEAPP_H

#include <iostream>
#include <string>
#include <limits>
#include <vector>
#include <regex>
#include <sstream>
#include <algorithm>

#include "deck.h"
#include "hand.h"
#include "discard.h"
#include "spread.h"

// Override Visual Studio macro definition for  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
#undef max

/* Overarching class for SFML version of Tonk that handles everything */
class TonkSFMLApp
{
	// This utility funciton will read in from standard input and verify that a number is input before continuing.
	// Will output a prompt to guide the user into entering in an integer
	int retrieveInt() const;

	// This function will return a number retrieved from standard input, and verify it's in the defined inclusive range before continuing
	int retrieveIntInRange(const int &min, const int &max) const;

	// Parses a list of comma delimted numbers into a vector and performs some error checking along the way
	int readNumberString(std::string str, std::vector<int> &ints);

	// Get player choice for spread
	int getPlayerChoiceForSpreadCreation();

	// This member function prompts the user to input the number of players.
	int retrieveNumPlayers();

	// Takes in a hand and processes that player's turn
	// Return 0 = proceed as normal, 1 = won, 2 = tonked, -1 = error
	int processTurn(Hand &hand);

	// Takes in a vector of hands and returns the index of the lowest hand
	int findIndexOfLowestHand(const std::vector<Hand> &hand) const;

	// Runs the game offline
	int runOffline();

	// Keeps track of number of players in the game
	int m_numplayers;

	// The main deck for the game
	Deck m_deck;
	// The discard pile for the game
	Discard m_discard;

	// Keeps track of all the hands.
	std::vector<Hand> m_hands;
	// Keeps track of all the spreads.
	std::vector<Spread> m_spreads;

	// Keeps track of whether the game is still running
	bool m_running;



public:
	TonkSFMLApp();
	// Runs the application. Returns 0 if succesful.
	int run();
};

#endif // TONKCONSOLEGAMEAPP_H
