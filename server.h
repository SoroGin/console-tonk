#ifndef SERVER_H
#define SERVER_H

#include "connection.h"

/* This class represents the server. The first player to start a game MUST be the server. */

class Server : public Connection
{
    // Used by server to listen for client connections
    SOCKET listenSocket;

public:
    Server();
    // Resolves address, creates and binds to socket
    int setupConnection();
    // Creates a socket that binds to the first available service provider that supports the requested combination of address family, socket type and protocol parameters
    int createSocket();
    // Associates a local address with a socket
    int bindSocket();
    // Places a bound socket in a state in which it is listening for an incoming connection
    int listenOnSocket();
    // Permits an incoming connection attempt on a socket
    int acceptClientConnection();
    // Closes socket. Use it to release the socket(s) associated with your connection object.
    int cleanup();
};

#endif // SERVER_H

