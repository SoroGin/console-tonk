#include "SFMLCardPile.h"

SFMLCardPile::SFMLCardPile() : SFMLCardPile(SFMLCardPile::SFMLCardPileInitializationMethod::asEmptyDeck) {
	m_orientation = SFMLCardPile::Orientation::FACE_UP;
	m_displaytype = SFMLCardPile::DisplayType::HORIZONTAL_SPREAD;
}

SFMLCardPile::SFMLCardPile(const SFMLCardPile::SFMLCardPileInitializationMethod SFMLCardPileInitMethod) {
	switch (SFMLCardPileInitMethod) {
	case SFMLCardPile::as52CardDeck:
		// Insert a card of each rank and suit into the deck
		// Iterate through suit
		for (int j = Card::Suit::Diamond; j <= Card::Suit::Spade; j++) {
			// Iterate through rank
			for (int i = Card::Rank::Ace; i <= Card::Rank::King; i++) {
				Card cardToInsert(Card::Card(static_cast<Card::Rank>(i), static_cast<Card::Suit>(j)));
				this->insertCardAtBack(cardToInsert);
			}
		}
		break;
	default:
		break;
	}

	this->setPosition(sf::Vector2f(0, 0));
}

void SFMLCardPile::insertCardAtBack(Card card) {
	this->m_cardpile.insertCardAtBack(card);
	this->m_cards.push_back(card);
	setPosition(m_position);
}

void SFMLCardPile::adjustCardsAsHorizontalSpread() {
	// 1/4 the width of texture, and multiply by scale of sprite
	for (int i = 0; i < m_cards.size(); i++) {
		int horizontalOffsetBetweenCards = m_cards[0].getTexture().getSize().x / 4 * .2;
		m_cards[i].setPosition(sf::Vector2f(m_position.x + i * horizontalOffsetBetweenCards, m_position.y));
	}
}

void SFMLCardPile::setPosition(sf::Vector2f position) {
	m_position = position;
	switch (m_displaytype) {
	case HORIZONTAL_SPREAD:
		this->adjustCardsAsHorizontalSpread();
		break;
	case VERTICAL_SPREAD:
		break;
	case FAN:
		break;
	case HORIZONTAL_STACKED:
		break;
	case VERTICAL_STACKED:
		break;
	default:
		std::cerr << "An error has occurred with detecting SFMLCardPile display type while setting position.\n";
		std::cerr << "The application will not exit.\n";
		exit(-1);
		break;
	}
}

SFMLCard SFMLCardPile::getSFMLCard(const int index) const {
	return m_cards[index];
}

void SFMLCardPile::scanMousePosition(const sf::Vector2i point) {
	for (int card = 0; card < m_cards.size(); card++) {
		m_cards[card].scanMousePosition(point);
	}
}

void SFMLCardPile::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// Draw from back to front if face up, otherwise draw from front to back, so it'll always look like [[[[[[]
	if (m_orientation == SFMLCardPile::Orientation::FACE_UP) {
		for (int card = 0; card < m_cards.size(); card++) {
			//states.texture = m_cards[card].getTexture();
			target.draw(m_cards[card], states);
		}
	}
	else if (m_orientation == SFMLCardPile::Orientation::FACE_DOWN) {

	}
}