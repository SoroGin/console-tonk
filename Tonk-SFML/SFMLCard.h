#ifndef SFMLCARD_H
#define SFMLCARD_H

#include <SFML/Graphics.hpp>
#include <Windows.h>

#include "card.h"

/*This is a class that implements an SFML drawable version of the Card class.*/

class SFMLCard : public sf::Drawable, public Card {
private:
	sf::Sprite m_sprite;
	sf::Texture m_texture;
	sf::RectangleShape m_glow;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public: 

	SFMLCard(Card card);
	SFMLCard(Card::Rank r, Card::Suit s);
	// Rule of three!:
	// Copy constructor
	SFMLCard(const SFMLCard& card);
	// Copy assignment operator
	SFMLCard& operator=(const SFMLCard& card);
	// Deconstructor
	~SFMLCard();
	void setPosition(const sf::Vector2f &pos);
	// Whether or not the card glows
	void setGlow(bool setToGlow);
	// Returns the globalBounds of the card
	sf::FloatRect getGlobalBounds() const;

	// Updates this class accordingly based on point given to it (mouse global position expected)
	void scanMousePosition(const sf::Vector2i point);

	sf::Vector2f getPosition() const;
	sf::Sprite getSprite() const;
	const sf::Texture &getTexture() const;
};

#endif // SFMLCARD_HB