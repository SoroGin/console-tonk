#ifndef CLIENT_H
#define CLIENT_H

#include "connection.h"

/* This class represents the client service. */

class Client : public Connection
{
public:
    Client(char* remoteIP);
    // Gets host ip and connects to socket
    int setupConnection();
    // Call the socket function and return its value to the ConnectSocket variable. Return 0 if successful, 1 otherwise.
    int connectToSocket();
    // Closes socket
    int cleanup();
};

#endif // CLIENT_H

