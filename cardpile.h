#ifndef CARDPILE_H
#define CARDPILE_H

#include <deque>
// Has, among other things, std::sort
#include <algorithm>
#include <iostream>

// For random seed generation
#include <stdlib.h>
#include <time.h>

// For object serializatoin
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/deque.hpp>

#include "card.h"

/* This is a general class that all future card piles, such as decks, hands, etc, will inherit from */
class CardPile
{
protected:
    // Ordered deque (heh) of cards in the deck
    std::deque<Card> m_cards;

public:
    // Starts with 0 cards
    CardPile();
    // Shuffles all cards. Returns 0 if successful, otherwise returns 1
    int shuffle();
    // Returns size of deck
    int size() const;
    // Returns the card at the top of the card pile
    Card top() const;
    // Deletes the card at the top of the card pile
    void pop_top();
    // Returns true of empty and false if not empty
    bool isEmpty() const;
    // Inserts a card into the card pile. Returns 0 on success and -1 on failure. Returns simply for inheritability reasons.
    virtual int insertCardAtFront(const Card &card);
    // Inserts a card at the end of the cardpile. Returns 0 on success and -1 on failure
    int insertCardAtBack(const Card &card);
    // Sorts the cardpile based on order - Suits first, then rank
    void sortByOrder();
    // Sorts the cardpile based on value - Ranks first, then suits
    void sortByValue();
    // Calculates total numeric value of the cardpile, A = 1, 2 = 2 ... J = 10, Q = 10, K = 10...
    int calculateNumericValue() const;
    // Returns card at given position
    Card at(int index) const;

    // Copies given cardpile to cardpile calling this function.
    void copy(const CardPile &pile);

    // Clears the cardpile completely
    void clear();

    // Prints all cards of card pile in order to console line by line
    virtual void print();

private:
    // Allow serialization to access non-public data members.
    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive& ar, const unsigned version)
    {
        ar & m_cards;  
    }
};

#endif // CARDPILE_H
