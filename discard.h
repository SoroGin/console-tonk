#ifndef DISCARD_H
#define DISCARD_H

#include "cardpile.h"

/* This class represents the discard pile in a game of Tonk */
class Discard : public CardPile
{
public:
    Discard();
};

#endif // DISCARD_H
