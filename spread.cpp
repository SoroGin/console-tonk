
#include "spread.h"

Spread::Spread()
{
    m_player = -1;
}

int Spread::insertCardAtBack(const Card &card) {
    int return_val = -1;
    // if the spread doesn't have any cards in it already, just add it to the spread and return success
    if(m_cards.size() == 0) {
        // Add to spread
        m_cards.push_back(card);
        // Set return flag to success
        return_val = 0;
    } else {
        // true if input card matches value of all cards currently in spread
        bool is_number_spread = true;
        // Check to see if it's a same number type spread
        for(unsigned i = 0; i < m_cards.size(); i++) {
            // If the given card is not the same rank as any particular card already in spread
            // then it fails the number_spread test
            if(!m_cards[i].isSameRank(card)) {
                is_number_spread = false;
            }
        }

        // Check to see if it's a run type spread (needs to be a run, ie 1-2-3, and same suit)

        // true if input card matches suit of all cards currently in spread
        bool suit_matches_all = true;
        // Check to see if card matches suit of all cards already in spread
        for(unsigned i = 0; i < m_cards.size(); i++) {
            // If the given card is not the same rank as any particular card already in spread
            // then it fails the number_spread test
            if(!m_cards[i].isSameSuit(card)) {
                suit_matches_all = false;
            }
        }

        // Check to see if if it's a proper run (ie 1-2-3 or 5-6-7)
        bool is_run = false;

        Card *lowest = new Card(Card::Rank::King, Card::Suit::Spade);
        Card *highest = new Card(Card::Rank::Ace, Card::Suit::Diamond);


        // find lowest and highest card currently in spread
        for(unsigned i = 0; i < m_cards.size(); i++) {
            if(m_cards[i].getVal() < lowest->getVal()) {
                lowest = &m_cards[i];
            }
            if(m_cards[i].getVal() > highest->getVal()) {
                highest = &m_cards[i];
            }
        }

        // All it needs to be is one removed from either the highest or lowest card already currently
        // in the spread to be verified.
        if(lowest->getRank() - 1 == card.getRank() || highest->getRank() + 1 == card.getRank()) {
            is_run = true;
        }

        if(is_run || is_number_spread) {
            m_cards.push_back(card);
            return_val = 0;
        }
    }

    return return_val;
}

// Change which player this spread belongs to (starts at index 0 for player 1). Does nothing if < 0 number given.
void Spread::setPlayer(const int player) {
    if(player < 0) {

    } else {
        m_player = player;
    }
}

// Get player number this spread belongs to, starting at index 0 for player 1. returns -1 when error (because m_player initializes to -1)
int Spread::getPlayer() const {
    return m_player;
}

