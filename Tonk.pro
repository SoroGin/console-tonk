TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    card.cpp \
    cardpile.cpp \
    deck.cpp \
    controller.cpp \
    tonkconsolegameapp.cpp \
    hand.cpp \
    discard.cpp \
    spread.cpp \
    client.cpp \
    connection.cpp \
    server.cpp \

HEADERS += \
    card.h \
    cardpile.h \
    deck.h \
    controller.h \
    tonkconsolegameapp.h \
    hand.h \
    discard.h \
    spread.h \
    client.h \
    connection.h \
    server.h \

# add boost
INCLUDEPATH += "V:\boost_1_59_0"
LIBS += "-LV:\boost_1_59_0\stage\lib"
