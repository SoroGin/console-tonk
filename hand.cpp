#include "hand.h"

Hand::Hand()
{
    this->isWinner = false;
}

Card Hand::getCard(int index) {
    Card temp = m_cards[index];
    m_cards.erase(m_cards.begin()+index);
    return temp;
}

Card Hand::at(int index) {
    return m_cards[index];
}

void Hand::setWinner() {
    this->isWinner = true;
}

bool Hand::getWinner() {
    return this->isWinner;
}

void Hand::print() {
    this->sortByValue();
    for(unsigned i = 0; i < this->m_cards.size(); i++) {
        std::cout << i+1 << ":" << this->m_cards[i] << " ";
    }
}
