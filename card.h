#ifndef CARD_H
#define CARD_H

// For overloading <<
#include <iostream>

// For object serializatoin
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

/* This class represents a playing card */
class Card
{
public:
    // Enumeration of all card ranks
    enum Rank {
        UninitializedRank = -1,
        Ace,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King
    };

    // Enumeration of all card suits
    enum Suit {
        UninitializedSuit = -1,
        Diamond,
        Club,
        Heart,
        Spade
    };

    Card();
    Card(Card::Rank r, Card::Suit s);

    // Returns card's suit
    Card::Suit getSuit() const;
    // Returns card's rank
    Card::Rank getRank() const;

    // Overloads << operator to allow easy printing of a card's rank and suit to console
    // It will print the card in the form (Rank, Suit)
    friend std::ostream& operator<<(std::ostream& os, const Card& card);

    // Returns the card's ordering value
    int getOrderingVal() const;
    // Returns the card's true value
    int getVal() const;
    // Returns the card's numeric value
    int getNumericValue() const;

    // Returns true if cards are same rank, otherwise returns false
    bool isSameRank(const Card &card) const;
    // Returns true if cards are same value, otherwise returns false
    bool isSameSuit(const Card &card) const;

    // Copies given card to the card calling this function
    virtual void copy(const Card &card);

// Private goes before public in this particular class because I can't figure out how the hell
// to forward declare enumerations
private:
    Card::Suit m_suit;
    Card::Rank m_rank;

    // Each card will have a unique value for ordering purposes
    // NOTE: This value is not indicative of actual value.
    int orderingVal;

    // Card's actual value
    int weightedVal;

    // Card's numeric value
    int numericVal;

    // Allow serialization to access non-public data members.
    friend class boost::serialization::access;
    template<typename Archive>
    void serialize(Archive& ar, const unsigned version)
    {
        ar & m_suit;
        ar & m_rank;
        ar & orderingVal;
        ar & weightedVal;
        ar & numericVal;
    }
};

// Provides a comparison function to pass to std::sort. Returns true if a is larger
// On a tie, it returns true
// NOTE: This is distinguished from actual value. Ex) Ace of Spades is higher than King of Hearts in order but not value.
bool largerByOrder(const Card& a, const Card& b);

// Provides a comparison function to pass to std::sort that returns true if a is larger by value
bool largerByValue(const Card& a, const Card& b);

#endif // CARD_H
