#include "connection.h"

Connection::Connection()
{
    connectSocket = INVALID_SOCKET;
    result = NULL;
    ip = NULL;

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;
}

char* Connection::getReceiveBuffer() {
    return receiveBuffer;
}

void Connection::clearBuffer() {
    memset(receiveBuffer, 0, DEFAULT_BUFFER_LEN);
}

int Connection::initialize() {
    // WSAStartup function is called to initiate use of WS2_32.dll
    int status = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (status != 0) {
        std::cout << "WSAStartup failed with error: " << status << std::endl;
        return 1;
    }
    std::cout << "Initialization completed." << std::endl;
    return 0;
}

int Connection::getMyIP() {
    DWORD dwError;
    hostent* localHost;

    // Stores information about a given host, such as host name and IPv4 address
    localHost = gethostbyname("");

    if (localHost == NULL) {
        dwError = WSAGetLastError();
        if (dwError != 0) {
            if (dwError == WSAHOST_NOT_FOUND) {
                std::cout << "Host not found." << std::endl;
                return 1;
            } else if (dwError == WSANO_DATA) {
                std::cout << "No data record found." << std::endl;
                return 1;
            } else {
                std::cout << "Function failed with error: " << dwError << std::endl;
                return 1;
            }
        }
    }

    ip = inet_ntoa(*(struct in_addr *)*localHost->h_addr_list);
    std::cout << "Your IP address is " << ip << std::endl;
    return 0;
}

int Connection::resolveAddress() {
    // getaddrinfo function populates a linked list of one or more addrinfo structures that contains response information about the host
    int status = getaddrinfo(ip, DEFAULT_PORT, &hints, &result);
    if ( status != 0 ) {
        std::cout << "getaddrinfo failed with error: " << status << std::endl;
        WSACleanup();
        return 1;
    }
    std::cout << "Host information acquired." << std::endl;
    return 0;
}

int Connection::sendMessage(const char *message) {
    // send message
    int sendStatus = send( connectSocket, message, (int)strlen(message), 0 );
    if (sendStatus == SOCKET_ERROR) {
        std::cout << "Send failed with error: " << WSAGetLastError() << std::endl;
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    // listen for acknowledgement that the message was received
    int receiveStatus = recv(connectSocket, receiveBuffer, DEFAULT_BUFFER_LEN, 0);
    if (receiveStatus < 0) {
        std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    // clear receiveBuffer
    clearBuffer();

    return 0;
}

int Connection::receiveMessage() {
    // Receive message
    int receiveStatus;
    receiveStatus = recv(connectSocket, receiveBuffer, DEFAULT_BUFFER_LEN, 0);
    if (receiveStatus < 0) {
        std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    // let the sender know that you've received their message
    int sendStatus = send(connectSocket, "ok", (int)strlen("ok"), 0);
    if (sendStatus == SOCKET_ERROR) {
        std::cout << "Send failed with error: " << WSAGetLastError() << std::endl;
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    return 0;
}

int Connection::shutdownConnection() {
    int status = shutdown(connectSocket, SD_SEND);
    if (status == SOCKET_ERROR) {
        std::cout << "shutdown failed with error: " << WSAGetLastError() << std::endl;
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    if(cleanup() > 0) {
        std::cerr << "ERROR: Unable to properly terminate the use of the Winsock 2 DLL (Ws2_32.dll)." << std::endl;
        return 1;
    }

    std::cout << "Connection successfully terminated. Goodbye." << std::endl;
    return 0;
}
