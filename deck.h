#ifndef DECK_H
#define DECK_H

#include "cardpile.h"
#include "card.h"

/* This class represents a deck of cards. It initializes with 52 ordered cards. */

class Deck : public CardPile
{
public:
    // The deck initializes with 52 cards.
    Deck();
};

#endif // DECK_H
