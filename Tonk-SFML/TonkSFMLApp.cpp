#include "TonkSFMLApp.h"

TonkSFMLApp::TonkSFMLApp()
{
	
}

// This current implementation of this function is not fully robust, as it allows for a case such as
// 123asdfajkdks and reads it as 123
int TonkSFMLApp::retrieveInt() const {
	int number;
	while (!(std::cin >> number)) {
		std::cin.clear();
		// QUESTION: This started complaining out of the blue. Well not completely out of the blue, it was after I hooked in the networking stuff.
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Sorry! That wasn't a valid input. Make sure to enter a number and try again!: ";
	}
	return number;
}

int TonkSFMLApp::retrieveNumPlayers() {
	// Decide how many players there will be
	std::cout << "How many players will be playing? (between 2-4): ";
	// Limit the number of players to a minimum of 2 but a maximum of 4
	// Retrieve an integer from the user and set it as numplayers
	return retrieveIntInRange(2, 4);
}

int TonkSFMLApp::retrieveIntInRange(const int &min, const int &max) const {
	int number = retrieveInt();
	while (number < min || number > max) {
		std::cout << "Sorry! That wasn't a valid choice. Try again!: ";
		number = retrieveInt();
	}
	return number;
}

int TonkSFMLApp::readNumberString(std::string str, std::vector<int> &ints) {
	// Verify that the input is a list of comma delimited integers. Spaces after commas are optional.
	if (!std::regex_match(str, std::regex("^(\\d+(,\\s?\\d+)*)$"))) {
		std::cout << "ERROR: Input string does not match expected pattern." << std::endl;
		return 1;
	}

	// Remove spaces from string. Helps avoid issues when parsing
	str.erase(remove_if(str.begin(), str.end(), isspace), str.end());

	// Split string using comma as delimiter and populate 'ints' vector
	std::stringstream ss(str);
	std::string item;
	while (std::getline(ss, item, ',')) {
		ints.push_back(atoi(item.c_str()));
	}

	std::sort(ints.begin(), ints.end());
	return 0;
}

int TonkSFMLApp::getPlayerChoiceForSpreadCreation() {
	std::cout << "Would you like to create a new spread(1), add to a spread(2), or continue(3)?";
	return retrieveIntInRange(1, 3);
}

int TonkSFMLApp::findIndexOfLowestHand(const std::vector<Hand> &hand) const {
	// Max hand value is 50 because 10 x 5 cards.
	// TO-DO: FIX THIS MAYBE. HAND SIZE MIGHT CHANGE DEPENDING ON NUMBER OF PLAYERS
	const int MAXHANDVALUE = 50;
	int lowestHand = MAXHANDVALUE;
	int indexOfLowestHand = -1;
	for (unsigned player = 0; player < hand.size(); player++) {
		int numericValueOfPlayer = hand[player].calculateNumericValue();
		if (lowestHand > numericValueOfPlayer) {
			lowestHand = numericValueOfPlayer;
			indexOfLowestHand = player;
		}
	}
	return indexOfLowestHand;
}

int TonkSFMLApp::runOffline() {
	m_numplayers = retrieveNumPlayers();

	std::cout << "Alright, I'll start the game for " << m_numplayers << " players!" << std::endl << std::endl;

	// Create the number of players and start tracking them
	for (int i = 0; i < m_numplayers; i++) {
		Hand newHand;
		m_hands.push_back(newHand);
	}

	// Shuffle the deck to prep it for play
	m_deck.shuffle();

	// Deal out the cards to the players
	for (unsigned player = 0; player < m_hands.size(); player++) {
		// Will deal out five cards
		for (int cardsToDeal = 0; cardsToDeal < 5; cardsToDeal++) {
			if (!m_deck.isEmpty()) {
				m_hands[player].insertCard(m_deck.top());
				m_deck.pop_top();
			}
			else {
				std::cerr << "ERROR: Something went wrong with the game. It tried to draw from an empty deck!" << std::endl;
			}
		}
	}

	// Start discard pile off with a card from the deck
	if (!m_deck.isEmpty()) {
		m_discard.insertCard(m_deck.top());
		m_deck.pop_top();
	}
	else {
		std::cerr << "ERROR: Something went wrong with the game. It tried to draw from an empty deck!" << std::endl;
	}

	// Whether or not the game is running
	m_running = true;

	// Loop until someone wins. 
	while (m_running) {
		for (unsigned player = 0; player < m_hands.size(); player++) {
			int playerNumber = player + 1;
			std::cout << "Hello player " << playerNumber << "." << std::endl;
			int whatToDoAfterTurn = processTurn(m_hands[player]);
			// TO-DO: If player has won, set m_running to false
			if (whatToDoAfterTurn == 1) {
				m_running = false;
			}
			else if (whatToDoAfterTurn == 2) {
				std::cout << "************************************************************" << std::endl;
				std::cout << "********************   You've tonked!   ********************" << std::endl;
				std::cout << "************************************************************" << std::endl;
				// print hands of each player
				for (unsigned player = 0; player < m_hands.size(); player++) {
					int playerNumber = player + 1;
					std::cout << "Player " << playerNumber << "'s hand was: " << std::endl;
					m_hands[player].print();
					std::cout << std::endl;
					std::cout << "And had a value of: " << m_hands[player].calculateNumericValue();
					std::cout << std::endl << std::endl;
				}
				int playerNumberWithLowestHand = findIndexOfLowestHand(m_hands) + 1;
				std::cout << "Player " << playerNumberWithLowestHand << " won!" << std::endl;
				return 0;
			}
		}
	}

	return 0;
}

int TonkSFMLApp::run() {
	// Welcome the player
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::*************   Welcome  **************::::" << std::endl;
	std::cout << "::::***************   To    ***************::::" << std::endl;
	std::cout << "::::*************    Tonk!   **************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;
	std::cout << "::::***************************************::::" << std::endl;

	std::cout << "Please say whether you'd like to play offline(1) or online(2): ";
	int offlineOnlineChoice = retrieveIntInRange(1, 2);

	// offline
	if (offlineOnlineChoice == 1) {
		runOffline();
	}
	int empty;
	std::cin >> empty;
	// TO-DO: Ask player if he / she wants to play again, and if so, relaunch the app
	return 0;
}

int TonkSFMLApp::processTurn(Hand &hand) {
	std::cout << ":::::::::::::::::::::::::::::::::::::::::::::" << std::endl << std::endl;
	// -1. Show the player his hand
	// Sort the player's hand first
	std::cout << "Your hand is: ";
	hand.print();
	std::cout << std::endl;

	// 0. Verify that neither the discard nor the deck is empty
	if (m_discard.isEmpty()) {
		// The discard pile should never be empty at this point, so throw an error if it is!
		std::cerr << "ERROR: For some reason, the discard pile has nothing in it!" << std::endl;
	}

	if (m_deck.isEmpty()) {
		// Handle the case for when the deck is empty!
		// When the deck runs out, it's an automatic Tonk! Lowest wins!
		return 2;
	}

	// 1. Draw a card either from discard pile or deck.
	std::cout << "The card that's face up on the discard pile is: " << m_discard.top() << std::endl << std::endl;
	std::cout << "Do you want to draw from the discard pile(1) or the deck(2), or tonk(3)?: ";

	int choice;
	// The choice can only be either 1, draw from discard, 2, draw from deck, or 3, tonk / drop
	while (!(choice = retrieveIntInRange(0, 3))) {
		std::cout << "Sorry! Try again, and make sure your choice is either draw from discard(1) or draw from deck(2) or tonk(3): ";
	}

	switch (choice) {
	case 1:
		// Give requested card to player
		std::cout << "Alright, I'll give you the card ";
		std::cout << "on the top of discard!: " << m_discard.top() << std::endl;
		// Player chose to draw from discard
		// Give the player the card
		hand.insertCard(m_discard.top());
		// Make sure the given card is no longer in the deck
		m_discard.pop_top();
		break;
	case 2:
		// Give requested card to player
		std::cout << "Alright, I'll give you the card ";
		std::cout << "from the top of the deck!: " << m_deck.top() << std::endl;
		// Player chose to draw from deck
		// Give the player the card
		hand.insertCard(m_deck.top());
		// Make sure the given card is no longer in the deck
		m_deck.pop_top();
		break;
	case 3:
		// Tonked
		return 2;
		break;
	default:
		std::cerr << "ERROR: Something went wrong when I tried to give you a card!" << std::endl;
		break;
	}
	\
		// 2. TO-DO: Handle creating any spreads player might want to create
		bool creatingSpread = true;
	while (creatingSpread) {
		int choice = getPlayerChoiceForSpreadCreation();
		std::vector<int> cardsForNewSpread;
		std::string stringOfCardsForNewSpread;
		Spread newCreatedSpread;
		bool cardsForNewSpreadOutOfRange = true;

		switch (choice) {
			// New spread
		case 1:
			std::cout << "Your hand is ";
			hand.print();
			std::cout << "Enter the cards you want to enter into a new spread: ";
			std::cin >> stringOfCardsForNewSpread;

			while (cardsForNewSpreadOutOfRange){
				readNumberString(stringOfCardsForNewSpread, cardsForNewSpread);
				for (unsigned i = 0; i < cardsForNewSpread.size(); i++) {
					if (cardsForNewSpread[i] < 0 || cardsForNewSpread[i] > 52) {
						std::cout << "An invalid number was entered! Please enter new number choices." << std::endl;
						cardsForNewSpread.clear();
						stringOfCardsForNewSpread.clear();
						std::cin >> stringOfCardsForNewSpread;
					}
					else {
						cardsForNewSpreadOutOfRange = false;
					}
				}
			}

			for (unsigned i = 0; i < cardsForNewSpread.size(); i++) {
				Card card(Card::Rank::Ace, Card::Suit::Spade);
				card = hand.at(cardsForNewSpread[i] - 1);
				if (newCreatedSpread.insertCard(card) == -1) {
					std::cout << "inserting a card failed. Reverting and breaking." << std::endl;
					int newCreatedSpreadSize = newCreatedSpread.size();
					for (int j = 0; j < newCreatedSpreadSize; j++) {
						hand.insertCard(newCreatedSpread.top());
						newCreatedSpread.pop_top();
					}
					break;
				}
				else {
					std::cout << "card inserted successfully" << std::endl;
					hand.getCard(cardsForNewSpread[i] - 1);
					// Adjust all the numbers in the cardsForNewSpread
					for (unsigned j = 0; j < cardsForNewSpread.size(); j++) {
						cardsForNewSpread[j]--;
					}
				}
			}
			newCreatedSpread.print();
			m_spreads.push_back(newCreatedSpread);
			break;
			// Add to pre-existing spread WARNING: THIS CODE MIGHT NOT WORK. HAS NOT BEEN TESTED
		case 2:
			std::cout << "Which spread would you like to add to?" << std::endl;
			int indexOfSpreadToAddCardsTo;
			for (unsigned i = 0; i < m_spreads.size(); i++) {
				std::cout << "Spread " << i;
				m_spreads[i].print();
			}
			indexOfSpreadToAddCardsTo = retrieveIntInRange(0, m_spreads.size());
			std::cout << "Your hand is ";
			hand.sortByValue();
			hand.print();
			std::cout << "Enter the cards you want to enter into a new spread: ";
			stringOfCardsForNewSpread.clear();
			std::cin >> stringOfCardsForNewSpread;
			cardsForNewSpreadOutOfRange = true;

			while (cardsForNewSpreadOutOfRange) {
				readNumberString(stringOfCardsForNewSpread, cardsForNewSpread);
				for (unsigned i = 0; i < cardsForNewSpread.size(); i++) {
					if (cardsForNewSpread[i] < 0 || cardsForNewSpread[i] > 52) {
						std::cout << "An invalid number was entered! Please enter new number choices." << std::endl;
						cardsForNewSpread.clear();
						stringOfCardsForNewSpread.clear();
						std::cin >> stringOfCardsForNewSpread;
					}
					else {
						cardsForNewSpreadOutOfRange = false;
					}
				}
			}

			for (unsigned i = 0; i < cardsForNewSpread.size(); i++) {
				Card card(Card::Rank::Ace, Card::Suit::Spade);
				card = hand.at(cardsForNewSpread[i] - 1);
				if (m_spreads[indexOfSpreadToAddCardsTo].insertCard(card) == -1) {
					std::cout << "inserting a card failed. Reverting and breaking." << std::endl;
					int newCreatedSpreadSize = m_spreads[indexOfSpreadToAddCardsTo].size();
					for (int j = 0; j < newCreatedSpreadSize; j++) {
						hand.insertCard(m_spreads[indexOfSpreadToAddCardsTo].top());
						m_spreads[indexOfSpreadToAddCardsTo].pop_top();
					}
					hand.sortByValue();
					break;
				}
				else {
					std::cout << "card inserted successfully" << std::endl;
					hand.getCard(cardsForNewSpread[i] - 1);
					// Adjust all the numbers in the cardsForNewSpread
					for (unsigned j = 0; j < cardsForNewSpread.size(); j++) {
						cardsForNewSpread[j]--;
					}
				}
			}

			break;
			// Don't create spread
		default:
			creatingSpread = false;
			break;
		}
	}

	// TO-DO: Add to pre-existing spread

	// 3. Discard a card.
	std::cout << "Choose a card to discard!" << std::endl;
	std::cout << "Your hand is: ";
	hand.print();

	// initialize
	while ((choice = retrieveInt()) < 0 || choice > hand.size()) {
		std::cout << "Sorry, that's not a valid number. Try again: ";
	}

	// Adjust player choice into appropriate hand index (which starts at 0)
	int hand_index = choice - 1;
	m_discard.insertCard(hand.getCard(hand_index));

	// Sort the player's hand
	hand.sortByValue();

	// 4. Check if player has won.
	if (hand.size() == 0) {
		std::cout << "You've won!" << std::endl;
		return 1;
	}

	return 0;
}
