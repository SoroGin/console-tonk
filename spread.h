#ifndef SPREAD_H
#define SPREAD_H

/* This is a class for handling spread creation. It will contain utility functions
 * that aid in the creation and handling of spreads */

#include "cardpile.h"

class Spread : public CardPile
{
public:
    Spread();

    // Will verify if it is a valid card to add to a spread. Returns 0 if okay and -1 if invalid spread.
    virtual int insertCardAtBack(const Card &card);

    // Change which player this spread belongs to (starts at index 0 for player 1)
    void setPlayer(const int player);

    // Get which player this spread belongs to (starts at index 0, returns -1 on error)
    int getPlayer() const;

private:
    // Which player this spread belongs to (index, so first player is 0)
    int m_player;

};

#endif // SPREAD_H
