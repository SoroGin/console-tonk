#include "deck.h"

Deck::Deck()
{
    // Insert a card of each rank and suit into the deck
    // Iterate through suit
    for(int j = Card::Suit::Diamond; j <= Card::Suit::Spade; j++) {
        // Iterate through rank
        for(int i = Card::Rank::Ace; i <= Card::Rank::King; i++) {
            this->m_cards.push_back(Card::Card(static_cast<Card::Rank>(i), static_cast<Card::Suit>(j)));
        }
    }
}
