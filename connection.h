#ifndef CONNECTION_H
#define CONNECTION_H

// Required Winsock directives
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <fstream>
#include <string>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define DEFAULT_BUFFER_LEN 1024
#define DEFAULT_PORT "27015"

/* Generic class from which server and client will inherit from */

class Connection
{
public:
    Connection();
    // Returns the array that is used to hold received message
    char* getReceiveBuffer();
    // Clears the receiveBuffer array to prepare for the next message
    void clearBuffer();
    // Initializes service and opens socket for communication
    virtual int setupConnection() = 0;
    // Guess what this does
    int sendMessage(const char *message);
    // Listen for incoming packets
    int receiveMessage();
    // Disable sending on a socket
    int shutdownConnection();
    // Closes socket(s). Number of sockets depends on connection type.
    virtual int cleanup() = 0;

protected:
    // Contains information about the Windows Sockets implementation
    WSADATA wsaData;
    // A descriptor referencing the new socket
    SOCKET connectSocket;
    // Linked list of one or more addrinfo structures that contains response information about the host
    struct addrinfo *result;
    // Contains hints about the type of socket the caller supports
    struct addrinfo hints;
    // Array to hold incoming message
    char receiveBuffer[DEFAULT_BUFFER_LEN];
    // Array to hold outgoing message
    char sendBuffer[DEFAULT_BUFFER_LEN];
    // IPv4 address
    char *ip;

    // Initialize Winsock. Returns 0 if successful, otherwise returns 1
    int initialize();
    // Finds local machine's IPv4 address. Returns 0 if successfull, 1 otherwise.
    int getMyIP();
    // Resolve the server address and port. Returns 0 if successful, otherwise returns 1
    int resolveAddress();

};

#endif // CONNECTION_H

